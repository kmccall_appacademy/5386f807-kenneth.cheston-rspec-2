def reverser
  block = yield
  block.split(" ").map(&:reverse!).join(" ")
end

def adder(number = 1)
  yield + number
end

def repeater(repeat = 1)
  repeat.times do
    yield
  end
end
