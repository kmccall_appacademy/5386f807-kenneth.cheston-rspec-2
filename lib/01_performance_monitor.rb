
require 'time'

@eleven_am = Time.parse("2011-1-2 11:00:00")

def measure(repeat = 1)
  start = Time.now
  repeat.times do
    yield
  end
  if repeat > 1
    return (Time.now - start) / repeat
  else
    return Time.now - start
  end
end
